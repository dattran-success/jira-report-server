'use strict';

module.exports = {
	env: 'development',
	db: {
		uri : 'mongodb://127.0.0.1/project_report',
		options : {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true
		},
		debug : false
	},
	dbTablePrefix : 'pr_',
	environment: 'dev',
	host: 'localhost',
	port : process.env.PORT || 3000,
	protocol: 'http',
	basePath: '/api/v1',
	appSecret: "a877d5917122583475477b1c53ff20d4ce53",
	// Session Cookie settings
	sessionCookie : {
		// session expiration is set by default to 24 hours
		maxAge : 7 * 24 * ( 60 * 60 * 1000 ),
		// httpOnly flag makes sure the cookie is only accessed
		// through the HTTP protocol and not JS/browser
		httpOnly : true,
		// secure cookie should be turned to true to provide additional
		// layer of security so that the cookie is set only when working
		// in HTTPS mode.
		secure : false
	},
	jwt: {
		secret: process.env.JWT_SECRET || 'EXPRESS-MVC',
		options: {
			expiresIn: '7d', // 1d
		},
		cookie: {
			// session expiration is set by default to 24h
			maxAge : 7 * 24 * ( 60 * 60 * 1000 ), // 7d
			httpOnly: true,
			secure: false
		}
	},
	// sessionSecret should be changed for security measures and concerns
	sessionSecret : process.env.SESSION_SECRET || 'EXPRESS-MVC',
	// sessionKey is set to the generic sessionId key used by PHP applications
	// for obsecurity reasons
	sessionKey : 'sessionId',
	sessionCollection : 'sessions',
	mailer : {
		from : "from@example.com",
		options : {
			host : "smtp.mailtrap.io",
			port : 2525,
			secure : true,
			pool: true,
			maxConnections: 1,
			auth : {
				user : "0fae663e5f6206",
				pass : "0b769cedbd98d3"
			}
		},
		admin : "dattran.software@gmail.com",
		error_log : "dattran.software@gmail.com"
	},
	pagination:{
		perPage: 10000
	},
	jira: {
		uri: "https://success-ss.atlassian.net",
		secret: "cGh1Yy5sZUBzdWNjZXNzc29mdHdhcmUuZ2xvYmFsOk5RcXREczJucGJocndIMEU5VnlyRDY5MA==",
		token_type: "Basic",
		limit: 100
	}
};
