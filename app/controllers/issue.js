'use strict';

let issueService = require('../services/issue');
let moment = require("moment");
let config = require('../../config');
let constants  = require('../resources/constants');

exports.getIssue = [
	(req, res, next) => {
		let origin_url = `${config.jira.uri}`;
		let url = origin_url;

		let created = null;
		if (req.query.crontab) {
			created = moment().subtract(1, 'days').format('YYYY/MM/DD');
		}

		let fields = [
			constants.FIELDS.ISSUE_TYPE,
			constants.FIELDS.STATUS,
			constants.FIELDS.SUMMARY,
			constants.FIELDS.PROJECT,
			constants.FIELDS.CREATED,
			constants.FIELDS.LABELS,
			constants.FIELDS.SPRINT,
			constants.FIELDS.WEIGHTED_DEFECT,
			constants.FIELDS.ESTIMATE_STORY_POINT
		];

		let method = constants.REQUEST_TYPE.GET;
		let limit = config.pagination.perPage;
		let max_result = config.jira.limit;
		let query = {};

		let options = {
			url,
			origin_url,
			method,
			limit,
			fields,
			created,
			max_result
		};
		
		issueService.createIssueFromAPI(query, options, (err, result) => {
			if(err) {
				return next(err);
			}

			res.json(result);
		});
	}
];
