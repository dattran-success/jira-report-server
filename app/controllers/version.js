'use strict';

let versionService = require('../services/version');
let constants  = require('../resources/constants');
let config = require('../../config');

exports.getVersions = [
	(req, res, next) => {
		let url = `${config.jira.uri}/rest/api/3/project`;
		let origin_url = url;
		let method = constants.REQUEST_TYPE.GET;
		let page = req.query.page;
		let noLimit = !!req.query.all;
		let limit = config.pagination.perPage;
		let sort = req.query.sort || '+name';
		let select = req.query.fields;
		let search = req.query.search;
		let query = {};
		let $and = [];

		let options = {
			url,
			origin_url,
			method,
			page,
			noLimit,
			limit,
			sort,
			select
		};
		let searchExp = { '$regex' : search, '$options' : 'i' };

		if(search) {
			let $or = [
				{
					name: searchExp
				}
			];

			if($and.length) {
				query.$and.push({ $or: $or });
			}
			else{
				query.$or = $or;
			}
		}
		versionService.createVersionFromAPI(query, options, async (err, result) => {
			if(err) {
				return next(err);
			}

			res.json(result);
		});
	}
];
