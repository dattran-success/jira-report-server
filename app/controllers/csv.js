'use strict';

let csvService = require('../services/csv');
let constants  = require('../resources/constants');

exports.upload = [
	(req, res, next) => {
		let query = {};
		let fields = [
			constants.FIELDS.ISSUE_TYPE,
			constants.FIELDS.STATUS,
			constants.FIELDS.SUMMARY,
			constants.FIELDS.PROJECT,
			constants.FIELDS.CREATED,
			constants.FIELDS.LABELS,
			constants.FIELDS.SPRINT,
			constants.FIELDS.WEIGHTED_DEFECT,
			constants.FIELDS.ESTIMATE_STORY_POINT
		];
		let options = {
			issues: JSON.parse(req.body.data_csv),
			fields
		};
		
		csvService.uploadCSV(query, options, (err, result) => {
			if(err) {
				return next(err);
			}

			res.json(result);
		});
	}
];
