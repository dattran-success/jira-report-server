'use strict';

let sprintService = require('../services/sprint');
let config = require('../../config');
let constants  = require('../resources/constants');

exports.getSprint = [
	(req, res, next) => {
		let url = `${config.jira.uri}`;
		let origin_url = url;
		let method = constants.REQUEST_TYPE.GET;
		let page = req.query.page;
		let limit = config.pagination.perPage;
		let query = {};

		let options = {
			url,
			origin_url,
			method,
			page,
			limit
		};
		
		sprintService.createSprintFromAPI(query, options, (err, result) => {
			if(err) {
				return next(err);
			}

		});

		res.json({ status: true, message: "Processing... data into Sprint Schema." });
	}
];
