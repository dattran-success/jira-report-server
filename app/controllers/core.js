'use strict';

let coreService = require('../services/core');
let userService = require('../services/user');
let config = require('../../config');

/**
 * Check user email exists
 */
exports.checkUserEmail = [
	(req, res, next) => {
		let email = req.params.email;

		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists
		userService.getUserByEmail(email, (err, user) => {
			// if there are any errors, return the error
			if (err) {
				return next(err);
			}

			res.json({ exists: !!user });
		});
	}
];
