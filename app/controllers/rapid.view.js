'use strict';
let rapidViewService = require('../services/rapid.view');
let constants  = require('../resources/constants');
let config = require('../../config');
let moment = require('moment');

exports.getRapidViews = [
	(req, res, next) => {
		let url = `${config.jira.uri}/rest/agile/1.0/board`;
		let origin_url = url;
		let method = constants.REQUEST_TYPE.GET;
		let query = {};
		let options = {
			url,
			origin_url,
			method
		};

		rapidViewService.getRapidViewByAPI(query, options, (error, response) => {
			if (error) {
				return next(error);
			}

			res.json(response);
		});
	}
];

exports.create = [
	(req, res, next) => {
		let query = {};
		let options = req.body;

		rapiViewService.createRapidView(query, options, (error, response) => {
			if (error) {
				return next(error);
			}

			res.json(response);
		});
	}
];
