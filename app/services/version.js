'use strict';

let Version = require('../models/version.model');
let baseService = require('./base');
let request = require("../helpers/request");

exports.upsert = ( id, update, next) => {
    baseService.findOneAndUpdate('Version', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (version, next) => {
	let newVersion = new Version(version);
	// set the user's local credentials
	newVersion.provider = 'local';

	newVersion.save((err) => {
		if (err) {
			return next(err);
		}
		if(next) {
			return next(null, newVersion);
		}
	});
};

exports.createVersionFromAPI = ( queries = {}, options, next) => {
	baseService.paginate('Project', queries, options, async (error, res) => {
		if (error) {
			return next(error);
		}

		let data = [];

		if (res) {
			for (let i = 0; i < res.data.length; i++) {
				options.url = `${options.origin_url}/${res.data[i].id}/versions`;

				let result = await request.initialize(options);

				if (result) {
					if (result.length > 0) {
						data = data.concat(result);
					}
				}

			}

			for (let j = 0; j < data.length; j ++) {
				this.upsert(data[j].id, data[j], (err, response) => {
					if (err) {
						console.log(err);
					}
				});
			}

			let callback = {
				status: true,
				message: `Processing... ${data.length} documents into VersionSchemas`
			};

			return next(null, callback);

		}
	});

}
