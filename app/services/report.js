'use strict';

let Report = require('../models/report.model');
let baseService = require('./base');
let moment = require('moment');
let request = require("../helpers/request");
let utils = require("../helpers/utils");
let constants = require("../resources/constants");

let getSprints = async(projectId) => {
	let query = {
        projectId: projectId,
        storyCompletionRatio: {
            $gt: 0
        }
    };
    let sort = {
        startDate: 1
    };

	return await baseService.query('Sprint', query).sort(sort);
};

let getStoriesDelivered = async(projectId) => {
    let query = [
        {
            $match: {
                'fields.project.id': projectId,
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.STORY, constants.ISSUES.DEFECT, constants.ISSUES.BUG]
                },
                'fields.status.name': {
                    $in: [constants.ISSUES.CLOSED, constants.ISSUES.DONE]
                }
            }
        },
        {
            $group: {
                _id: {
                    id: '$sprint.id', 
                    name: '$sprint.name',
                    startDate: '$sprint.startDate'
                },
                story_delivered: {
                    $sum: `$fields.${constants.FIELDS.ESTIMATE_STORY_POINT}`
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id.id',
                name: '$_id.name',
                story_delivered: '$story_delivered',
                startDate:'$_id.startDate'
            }
        },
        {  
            $sort: { startDate: 1 } 
        }
    ];

    if (projectId !== 14261) {
        query.length = 3;
        delete query[2].$project.startDate;
        delete query[1].$group._id.startDate;
    }

    return await baseService.aggregate('Issue', query);
};

let getWeightedDefect = async(projectId) => {
    let query = [
        {
            $match: {
                'fields.project.id': projectId,
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.DEFECT, constants.ISSUES.STORY, constants.ISSUES.BUG]
                }
            }
        },
        {
            $group: {
                _id: {
                    id: '$sprint.id', 
                    name: '$sprint.name',
                    startDate: '$sprint.startDate'
                },
                weighted_defect: {
                    $sum: `$fields.${constants.FIELDS.WEIGHTED_DEFECT}`
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id.id',
                name: '$_id.name',
                weighted_defect: '$weighted_defect',
                startDate:'$_id.startDate'
            }
        },
        {  
            $sort: { startDate: 1 } 
        }
    ];

    if (projectId !== 14261) {
        query.length = 3;
        delete query[2].$project.startDate;
        delete query[1].$group._id.startDate;
    }

    return await baseService.aggregate('Issue', query);
};

let getTotalEstimatedStoryPoint = async(projectId) => {
    let query = [{
        $match: {
            'fields.project.id': projectId,
            'fields.issuetype.name': {
                $in: [constants.ISSUES.DEFECT, constants.ISSUES.STORY, constants.ISSUES.BUG]
            }
        }
    },
    {
        $group: {
            _id: '$fields.created',
            total_estimated_story_point: {
                $sum: `$fields.${constants.FIELDS.ESTIMATE_STORY_POINT}`
            }
        }
    }
    ];

    let sort = {
        '_id': 1
    };

    return await baseService.aggregate('Issue', query).sort(sort);
}

let getTotalStoryPointResolved = async(projectId) => {
    let query = [
        {
            $match: {
                'fields.project.id': projectId,
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.DEFECT, constants.ISSUES.STORY, constants.ISSUES.BUG]
                },
                'fields.status.name': {
                    $in: [constants.ISSUES.CLOSED, constants.ISSUES.DONE]
                }
            }
        },
        {
            $group: {
                _id: '$fields.created',
                total_story_point_resolved: {
                    $sum: `$fields.${constants.FIELDS.ESTIMATE_STORY_POINT}`
                }
            }
        }
    ];

    let sort = {
        '_id': 1
    };

    return await baseService.aggregate('Issue', query).sort(sort);
};

let getReleaseVersionLatest = async(projectId) => {

    let query = {
        projectId: projectId,
        released: true
    };

	return await baseService.query('Version', query)
};

let maxReleaseDate = async (arrDateVersion) => {
    if (arrDateVersion.length > 0) {
        let arrays = [];
        arrDateVersion.forEach(element => {
            arrays.push(element.releaseDate);
        });
        
        let moments = arrays.map(d => moment(d));
        let maxDate = moment.max(moments);
        return arrDateVersion.filter(function(element){ return element.releaseDate == maxDate.format('YYYY-MM-DD') });
        
    }
    return null;
};

let getAllWeightedDefectByDate = async(projectId) => {
    let query = [
        {
            $match: {
                'fields.project.id': projectId
            }
        },
        {
            $group: {
                _id: {
                    id: '$sprint.id',
                    name: '$sprint.name',
                    created: '$fields.created'
                },
                defect_count: {
                    $sum: 1
                },
                weightted_defect: {
                    $sum: `$fields.${constants.FIELDS.ESTIMATE_STORY_POINT}`
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id.id',
                name: '$_id.name',
                defect_count: '$defect_count',
                weighted_defect: '$weightted_defect',
                created: '$_id.created'
            }
        },
        {  
            $sort: { created: 1 }
        }
    ];

    return await baseService.aggregate('Issue', query);
};

let calcDefectDensity = async(storyPoint, weightedDefect) => {
    let result = 0;
    if (storyPoint !== 0) {
        result = weightedDefect / storyPoint;
    }
    return parseFloat(result.toFixed(2));
}

let calcVelocity = async(initialVelocity, avarageVelocity) => {
    let result = 0;
    if (initialVelocity !== 0) {
        result = ((avarageVelocity - initialVelocity) / initialVelocity) / 100;
    }
    return parseFloat(result.toFixed(3));
}

let calcVelocityVariance = async(recentSprintVelocity, avarageVelocity) => {
    let result = 0;
    if (avarageVelocity !== 0) {
        result = (recentSprintVelocity - avarageVelocity) / avarageVelocity;
    }
    return parseFloat(result.toFixed(3));
}

exports.handleReport = async(project) => {
	let sum = 0,
        sumWeightedDefect = 0,
        sumStoryPoint = 0,
        weightedDefect = 0,
        storyPoint = 0,
        sumWeightedDefectByDate = 0,
        calcWeightedDefect = 0,
        sizeSprint = 0,
        version = null,
        releaseDateLatest = null,
        defectDensityByDate = [],
        defectDensity = [],
        velocityIncrease = [],
        averageVelocity = [],
        summaryData = [];
    let storiesCompletionRatio = await getSprints(project.id);
    let deliveredAndSpData  = await getStoriesDelivered(project.id);
    let weightedDefectData = await getWeightedDefect(project.id);
    let totalEstimatedStoryPointData = await getTotalEstimatedStoryPoint(project.id);
    let totalStoryPointResolvedData = await getTotalStoryPointResolved(project.id);

    
    let updateAt = moment().valueOf();
    let getReleaseDate = await getReleaseVersionLatest(project.id);
    let getMaxReleaseDate = await maxReleaseDate(getReleaseDate);
    let getAllWeightedDefect = await getAllWeightedDefectByDate(project.id);
    let totalSizeSprint = deliveredAndSpData;

    if (project.id !== 14261) {
        deliveredAndSpData = utils.filterArray(deliveredAndSpData);
        weightedDefectData = utils.filterArray(weightedDefectData);
    }

    if(getMaxReleaseDate != null) {
        version = getMaxReleaseDate[0].name ;
        releaseDateLatest = moment(getMaxReleaseDate[0].releaseDate).valueOf();
    }
    
    // calc defect density
    if (weightedDefectData.length > 0) {
        for (const item of weightedDefectData) {
            for (const point of deliveredAndSpData) {
                if (point.id == item.id &&
                    item.id != null &&
                    point.id != null
                ) {
                    storyPoint = point.story_delivered;
                    weightedDefect = item.weighted_defect;
                    sumWeightedDefect += item.weighted_defect;
                    sumStoryPoint += storyPoint;
    
                    defectDensity.push({
                        sprint: item.name,
                        weightedDefect: weightedDefect,
                        sp: storyPoint,
                        defectDensity: await calcDefectDensity(storyPoint, weightedDefect),
                        accumulatedDefectDensity: await calcDefectDensity(sumStoryPoint, sumWeightedDefect)
                    });
                }
            }
        }
    }
    
    // calc velocity increase 
    if (deliveredAndSpData.length > 0) {
        for (let i = 0; i < deliveredAndSpData.length; i++) {
            averageVelocity.push(deliveredAndSpData[i].story_delivered);
    
            sum += deliveredAndSpData[i].story_delivered;
            let avg = parseFloat((sum / averageVelocity.length).toFixed(2));
            let velocity = await calcVelocity(deliveredAndSpData[0].story_delivered, avg);
            let variance = await calcVelocityVariance(deliveredAndSpData[i].story_delivered, avg);
    
            velocityIncrease.push({
                sprint: deliveredAndSpData[i].name,
                storyDelivered: deliveredAndSpData[i].story_delivered,
                averageVelocity: avg,
                velocityIncrease: velocity,
                velocityVariance: variance
            });
        }
    }
    
    // custom data total story point resolved
    if (totalEstimatedStoryPointData.length > 0) {
        for (const estimatedStory of totalEstimatedStoryPointData) {
            let total_story_point_resolved = 0
            // custom data total story point resolved
            for (const resolvedStory of totalStoryPointResolvedData) {
                if (estimatedStory._id == resolvedStory._id) {
                    total_story_point_resolved = resolvedStory.total_story_point_resolved
                }
            } 
            summaryData.push({
                date: estimatedStory._id,
                total_estimated_story_point: estimatedStory.total_estimated_story_point,
                total_story_point_resolved : total_story_point_resolved
            });
        }
    }
    
    // calc weightDefect
    if (getAllWeightedDefect.length > 0) {
        for(const item of getAllWeightedDefect) {
            for(const point of totalSizeSprint) {
                if (point.id == item.id && item.id != null && point.id != null) {
                    sizeSprint = point.story_delivered;
                    calcWeightedDefect = item.weighted_defect;
                    sumWeightedDefectByDate += item.weighted_defect;
                    
                    defectDensityByDate.push({
                        date: item.created,
                        defectCount: item.defect_count,
                        weightedDefect: calcWeightedDefect,
                        accumulatedDefectDensity: sumWeightedDefectByDate,
                        defect_density: await calcDefectDensity(sizeSprint, sumWeightedDefectByDate),
                    });
                }
            }
        }
    }

    let data = {
        id: project.id,
        project: project.name,
        flag: project.flag,
        summaryData: summaryData,
        storiesCompletionRatio,
        defectDensity,
        velocityIncrease,
        updateAt,
        version,
        releaseDateLatest,
        defectDensityByDate,
        deliveredAndSpData
    };

    this.upsert(project.id, data, (err, reponse) => {
        if (err) {
            console.log(err);
        }
    });
}

exports.upsert = (id, update, next) => {
	return baseService.findOneAndUpdate('Report', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (report, next) => {
	let newReport = new Report(report);
	// set the user's local credentials
	newReport.provider = 'local';

	newReport.save((err) => {
		if (err) {
			return next(err);
		}
		if (next) {
			return next(null, newReport);
		}
	});
};

exports.getReport = (queries = {}, options, next) => {
	baseService.paginate('Project', queries, options, async(error, res) => {
		if (error) {
			return next(error);
		}
		
		if (res && res.data) {
			
			for (let item of res.data) {
                this.handleReport(item);
			}
			
			let callback = {
				status: true,
				message: `Processing... documents into Report Schema`
			};

			return next(null, callback);
		}
	});
}

