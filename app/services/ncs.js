'use strict';

let Report = require('../models/report.model');
let baseService = require('./base');
let moment = require('moment');
let request = require("../helpers/request");
let utils = require("../helpers/utils");
let constants = require("../resources/constants");
let _ = require('lodash');

let getSprints = async(projectId) => {
	let query = {
		projectId: projectId
    };
    let sort = {
        startDate: 1
    };

	return await baseService.query('Sprint', query).sort(sort);
};

let getStoriesDelivered = async(projectId) => {
    let query = [
        {
            $match: {
                'fields.project.id': projectId,
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.STORY, constants.ISSUES.DEFECT, constants.ISSUES.BUG]
                },
                'fields.status.name': {
                    $in: [constants.ISSUES.CLOSED, constants.ISSUES.DONE]
                }
            }
        },
        {
            $group: {
                _id: {
                    id: '$sprint.id', 
                    name: '$sprint.name',
                    startDate: '$sprint.startDate'
                },
                story_delivered: {
                    $sum: `$fields.${constants.FIELDS.ESTIMATE_STORY_POINT}`
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id.id',
                name: '$_id.name',
                story_delivered: '$story_delivered',
                startDate:'$_id.startDate'
            }
        },
        {  
            $sort: { startDate: 1 } 
        }
    ];

    if (projectId !== 14261) {
        query.length = 3;
        delete query[2].$project.startDate;
        delete query[1].$group._id.startDate;
    }

    return await baseService.aggregate('Issue', query);
};

let countNCByStatus = async() => {
    let query = [{
        $match: {
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.NC]
                },
            }
        },
        
        {
            $group: {
                _id: {
                    projectId:`$fields.project.id`,
                    projectName: '$fields.project.name',
                    status: `$fields.status.name`
                },
                total:{
                    $sum:1
                },
            }
        },
        {
            $project: {
                _id:0,
                status: '$_id.status',
                project_id: '$_id.projectId',
                project_name: '$_id.projectName',
                total: '$total',
                
            }
        }
    ];

    let sort = {
        '_id': 1
    };

    return await baseService.aggregate('Issue', query).sort(sort);

};

let countNCByPracticeArea = async() => {
    let query = [{
        $match: {
                'fields.issuetype.name': {
                    $in: [constants.ISSUES.NC]
                },
            }
        },
        
        {
            $group: {
                _id: {
                    projectId:`$fields.project.id`,
                    projectName: '$fields.project.name',
                    labels: `$fields.labels`
                },
                total:{
                    $sum:1
                },
            }
        },
        {
            $project: {
                _id:0,
                labels: '$_id.labels',
                project_id: '$_id.projectId',
                project_name: '$_id.projectName',
                total: '$total',
                
            }
        }
    ];

    let sort = {
        '_id': 1
    };

    return await baseService.aggregate('Issue', query).sort(sort);
}

let handleNCs = async(project, dataStatus, dataLabels) => {
    let statusObj = {};
    let labelsObj = {};
    
    for (let row of dataStatus) {
        if (row.project_id == project.id) {
            let status = row.status;
            if (!(status in statusObj)) {
                statusObj[status] = row.total;
            }
        }
    }

    delete statusObj.rapidViewId;
    
    for (let row of dataLabels){
        if (row.project_id == project.id) {
            let labels = row.labels; 
            if (labels !== undefined && labels.length) {
                let label = labels.filter(x => typeof x!== undefined).shift();
                if (label && !(label in statusObj)) {
                    labelsObj[label] = row.total;
                }
            }
            
        }
    }

    if (_.isEmpty(statusObj) && _.isEmpty(labelsObj)) return;

    let data = {
        id: project.id,
        project: project.name,
        status: statusObj,
        area: labelsObj
    };

    this.upsert(project.id, data, (err, reponse) => {
        if (err) {
            console.log(err);
        }
    });
}

exports.upsert = (id, update, next) => {
	return baseService.findOneAndUpdate('NCs', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.getNCs = (queries = {}, options, next) => {
	baseService.paginate('Project', queries, options, async(error, res) => {
		if (error) {
			return next(error);
		}
		
		if (res && res.data) {
			let dataStatus = await countNCByStatus();
            let dataLabels = await countNCByPracticeArea();

			for (let item of res.data) {
                handleNCs(item, dataStatus, dataLabels);
			}
			
			let callback = {
				status: true,
				message: `Processing... documents into NCsSchema`
			};

			return next(null, callback);
		}
	});
}

