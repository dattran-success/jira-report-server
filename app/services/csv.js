'use strict';

let Report = require('../models/report.model');
let baseService = require('./base');
let issueService = require('./issue');
let sprintSerice = require('./sprint');
let reportService = require('./report');
let moment = require('moment');
let request = require("../helpers/request");
let utils = require("../helpers/utils");
let constants = require("../resources/constants");

let getArraySprints = (data, arraySprint = []) => {
    // get array name sprint
    arraySprint = data.reduce((r, a) => {
        r[a.sprint.name] = a.sprint.name;
        return r;
    }, {});

    // convert obj to array
    arraySprint = Object.entries(arraySprint)
        .reduce((acc, it) => [...acc, ...it], []);
    arraySprint = [...new Set(arraySprint)];

    // remove sprint is null or 'null'
    arraySprint = arraySprint.filter(it => it != null && it != 'null');

    return arraySprint.sort();
}

let mapDataSprint = async(data, arraySprint, dataSprints) => {
    dataSprints = arraySprint.map((item, index) => {
        let countStoryOfCompletedIssues = 0;
        let countStoryOfCommittedIssues = 0;
        let storyCompletionRatio = 0;
        let id = moment(moment().add( index, 'days').format('YYYY-MM-DD HH:mm:ss')).valueOf();
        let sprint = {
            id: id,
            sprintId: id,
            sprintName: item,
            startDate: id
        };

        for (let p = 0; p < data.length; p++) {
            if ( item == data[p].sprint.name && data[p].fields.issuetype.name == constants.ISSUES.STORY) {
                countStoryOfCommittedIssues = countStoryOfCommittedIssues + 1;
                if (data[p].fields.status.name == constants.ISSUES.DONE) {
                    countStoryOfCompletedIssues = countStoryOfCompletedIssues + 1;
                }
            }

            if (!sprint.projectId) {
                sprint.projectId = data[p].fields.project.id;
                sprint.projectName = data[p].fields.project.name;
            }

            sprint.completedIssues = countStoryOfCompletedIssues;
            sprint.committedIssues = countStoryOfCommittedIssues;
        }

        if (countStoryOfCommittedIssues > 0) {
            storyCompletionRatio = parseFloat((countStoryOfCompletedIssues / countStoryOfCommittedIssues * 100).toFixed(2));
        }

        sprint.storyCompletionRatio = storyCompletionRatio;

        return sprint;
    });

    return dataSprints;
}

exports.uploadCSV = async(queries = {}, options, next) => {
    let data = options.issues.attributes;
    let projectId = options.issues.projectId;
    let arraySprint = await getArraySprints(data, []);
    let dataSprints = await mapDataSprint(data, arraySprint, []);
    let timeout = null;

    for (let sprint of dataSprints) {
        let find = await baseService.query('Sprint', { sprintName: sprint.sprintName });

        if (find && find.length > 0) {
            break;
        }
        
        sprintSerice.upsert(sprint.id, sprint, (err, res) => {
            if (err) {
                console.log(err);
            }
        });

        timeout = await utils.sleep(500);
    }

    for (let item of data) {
        // handle weighted defect
        if (!item.fields[options.fields[7]] && item.fields[options.fields[7]] == null) {
            item.fields[options.fields[7]] = 0;
        }

        // handle estimate story point
        if (!item.fields[options.fields[8]] && item.fields[options.fields[8]] == null) {
            item.fields[options.fields[8]] = 0;
        }

        if (!item.sprint.id || item.sprint.id == null) {
            let findSprint = await baseService.query('Sprint', { sprintName: item.sprint.name });

            if (findSprint && findSprint.length > 0) {
                findSprint = findSprint[0];
                item.sprint.id = findSprint.sprintId;
                item.sprint.startDate = findSprint.startDate;
            }
        }

        let issue = await baseService.query('Issue', { key: item.key });

        if (issue && issue.length > 0) {
            break;
        }
        
        issueService.upsert(item.id, item, async (err, response) => {
            if (err) {
                console.log(err);
            }

            timeout = await utils.sleep(10);
        });
    }

    let project = await baseService.query('Project', { id: projectId });

    if (project && project.length > 0) {
        await reportService.handleReport(project[0]);
    }

    clearTimeout(timeout);

    return next(null, { success: true, data: [] });
}

