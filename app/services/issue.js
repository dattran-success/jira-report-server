'use strict';

let Issue = require('../models/issue.model');
let baseService = require('./base');
let moment = require('moment');
let request = require("../helpers/request");
let utils = require("../helpers/utils");

exports.upsert = (id, update, next) => {
	return baseService.findOneAndUpdate('Issue', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (issue, next) => {
	let newIssue = new Issue(issue);
	// set the user's local credentials
	newIssue.provider = 'local';

	newIssue.save((err) => {
		if (err) {
			return next(err);
		}
		if (next) {
			return next(null, newIssue);
		}
	});
};

exports.createIssueFromAPI = (queries = {}, options, next) => {
	baseService.paginate('Project', queries, options, async(error, res) => {
		if (error) {
			return next(error);
		}

		if (res && res.data) {
			let data = [];

			for (let i = 0; i < res.data.length ; i++) {
				let startAt = 0;
				let params = {
					project: res.data[i].id,
					startAt: startAt,
					maxResults: options.max_result,
					fields: options.fields.join(',')
				};

				if (options.created) {
					params.project = `${res.data[i].id} and created >= "${options.created}"`
				}
				
				options.url = `${options.origin_url}/rest/api/3/search?jql=${utils.convertObjToURLParams(params)}`;
				let result = await request.initialize(options);
				if (result == null) {
					continue;
				}

				if (result && result.total > 0) {

					let totalLoop = Math.ceil(result.total / options.max_result);
					data = data.concat(result.issues);

					if (totalLoop > 1) {
						for (let j = 1; j < totalLoop; j++) {
							params.startAt += options.max_result;
							options.url = `${options.origin_url}/rest/api/3/search?jql=${utils.convertObjToURLParams(params)}`;

							let response = await request.initialize(options);
							if (response == null) {
								continue;
							}

							if (response) {
								data = data.concat(response.issues);
							}
						}
					}
				}
			}

			for (let item of data) {

				let dataSprint = {};
				// handle date
                let date = moment(item.fields[options.fields[4]]).format('YYYY-MM-DD');
                // set created by timestamps
                item.fields[options.fields[4]] = moment(date).valueOf();

				// handle weighted defect
				if (!item.fields[options.fields[7]] && item.fields[options.fields[7]] == null) {
					item.fields[options.fields[7]] = 0;
				}

				// handle estimate story point
				if (!item.fields[options.fields[8]] && item.fields[options.fields[8]] == null) {
					item.fields[options.fields[8]] = 0;
				}

				// handle sprint 
				if (item.fields[options.fields[6]] && item.fields[options.fields[6]] != null) {
                    let sprint = item.fields[options.fields[6]];
					let last = sprint[sprint.length - 1];
                    // slice string of custom field sprint
                    sprint = last.slice(last.indexOf('[') + 1, last.length - 1);
                    sprint = sprint.split(',');

                    for (let str of sprint) {

                        // trim data
                        str = str.trim();
                        // get index '=' character
                        let i = str.indexOf('=');
                        // attributes available
                        let allowData = ['name', 'id', 'state','startDate'];

                        if (allowData.includes(str.slice(0, i))) {
                            // set key and value obj
                            dataSprint[str.slice(0, i)] = str.slice(i + 1, str.length);
                        }

					}

					if (dataSprint.startDate == "<null>") {
						let findSprint = await baseService.query('Sprint', { id: dataSprint.id });

						if (findSprint && findSprint.length > 0) {
							dataSprint.startDate = findSprint[0]['startDate'];
						}
							
					} else {
						let startDate = moment(dataSprint.startDate).format('YYYY-MM-DD');
						dataSprint.startDate = moment(startDate).valueOf();
						
					}

					item.sprint = dataSprint;

				} else {
					item.sprint = {};
				}

				this.upsert(item.id, item, (err, response) => {
					if (err) {
						console.log(err);
					}
				});
			}

			let callback = {
				status: true,
				message: `Processing... documents into Issue Schema`
			};

			return next(null, callback);
		}
	});
}

