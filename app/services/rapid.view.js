'use strict';

let RapidView = require('../models/rapid.view.model');
let baseService = require('./base');
let reportService = require('./report');
let projectService = require('./project');
let request = require('../helpers/request');
let utils = require('../helpers/utils');
let moment = require('moment');

let getProjectByName = async(project) => {
	let query = {
		name: project
    };

	return await baseService.query('Project', query);
};

exports.upsert = ( id, update, next) => {
    baseService.findOneAndUpdate('RapidView', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (rapidView, next) => {
	let newRapidView = new RapidView(rapidView);
	// set the user's local credentials
	newRapidView.provider = 'local';

	newRapidView.save((err) => {
		if (err) {
			return next(err);
		}
		if(next) {
			return next(null, newRapidView);
		}
	});
};

exports.get = (queries = {}, options, next) => {
	if(options.noLimit) {
		let sort = options.sort || '-created_time'; // "-created_time"
		let order = sort.substring(0, 1);
		let field = sort.substring(1);

		return baseService.find('RapidView', queries, options.select, {
			sort: {
				[ field ]:  order === '+' ? +1 : -1
			}
		}, next);
	}

	return baseService.paginate('RapidView', queries, options, next );
};

exports.getRapidViewByAPI = async (queries = {}, options, next) => {
	let result = await request.initialize(options);

	if (result) {
		// get calc total page
		let totalPage = Math.ceil(result.total / result.maxResults) || 0;
		let updateData = result.values.filter(el => el.location.projectId > 14260);

		// get rest data other page
		if (totalPage > 1) {
			for (let i = 2; i <= totalPage; i++) {
				// calc startAt value
				let startAt = (i - 1) * result.maxResults;
				// update url with startAt
				options.url = `${options.origin_url}?startAt=${startAt}`;

				// get data with new startAt value
				let response = await request.initialize(options);

				if (response) {
					response.values = response.values.filter(el => el.location.projectId > 14260);
					
					// join updateData with new response
					updateData = updateData.concat(response.values);

				}

			}
		}

		for (let j = 0; j < updateData.length; j ++) {
			this.upsert(updateData[j].id, updateData[j], (errorUpdate, updated) => {
				if (errorUpdate) {
					console.log(errorUpdate);
				}
			});
		}

		// response data to finish
		let updated = {
			status: true,
			message: `Processing... ${updateData.length} documents into RapidViewSchemas.`
		};

		return next(null, updated);
	}
};

exports.createRapidView = async(queries = {}, options, next) => {
	let projects = await getProjectByName(options.project);
	let flag = utils.customizeName(options.project);
	
	if (projects.length == 0) {
		let data = {
			id: moment(moment().format('YYYY-MM-DD HH:mm:ss')).valueOf(),
			flag: flag,
			key: utils.customizeName(options.project, true),
			name: options.project,
			rapidViewId: 0,
			rapidView: [],
			active: true
		};

		projectService.upsert(data.id, data, async (err, response) => {
			if (err) {
				return next(err);
			}

			if (response) {
				await reportService.handleReport(data);
				return next(null, {
					success: true,
					data: data.flag
				});
			}
		});

	} else {
		await reportService.handleReport(projects[0]);
		return next(null, {
			success: true,
			data: flag
		});
	}
};
