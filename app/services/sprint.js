'use strict';

let Sprint = require('../models/sprint.model');
let baseService = require('./base');
let moment = require('moment');
let request = require("../helpers/request");

let getDataSprint = (data, sprint, callback) => {
	let completedIssues = data.contents.completedIssues,
		issuesRemovedFromSprint = data.contents.puntedIssues,
		countStoryOfCompletedIssues = 0,
		countStoryOfIssuesRemovedFromSprint = 0;

	completedIssues.forEach(completedIssue => {
		if(completedIssue.typeName == "Story") {
			countStoryOfCompletedIssues ++;
		}
	});

	issuesRemovedFromSprint.forEach(notCompletedIssue => {
		if(notCompletedIssue.typeName == "Story") {
			countStoryOfIssuesRemovedFromSprint ++;
		}
	});

	let committedIssues = countStoryOfCompletedIssues + countStoryOfIssuesRemovedFromSprint;
	let storyCompletionRatio = committedIssues == 0 
		? 0
		: parseFloat((countStoryOfCompletedIssues/committedIssues*100).toFixed(2));
	
	// if (storyCompletionRatio == 0) return;

	let dataUpdate = {
		id: sprint.id,
		projectId: sprint.projectId,
		projectName: sprint.projectName,
		sprintName: sprint.name,
		sprintId: sprint.id,
		completedIssues: countStoryOfCompletedIssues,
		committedIssues: committedIssues,
		storyCompletionRatio: storyCompletionRatio,
		startDate: moment(sprint.startDate).unix(),
		rapidViewId: sprint.originBoardId
	}
	this.upsert(sprint.id, dataUpdate, callback);
}

exports.upsert = (id, update, next) => {
	return baseService.findOneAndUpdate('Sprint', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (sprint, next) => {
	let newSprint = new Sprint(sprint);
	// set the user's local credentials
	newSprint.provider = 'local';

	newSprint.save((err) => {
		if (err) {
			return next(err);
		}
		if (next) {
			return next(null, newSprint);
		}
	});
};

exports.createSprintFromAPI = (queries = {}, options, next) => {
	baseService.paginate('Project', queries, options, async(error, res) => {
		if (error) {
			return next(error);
		}
		
		if (res && res.data) {
			let data = [];
			for (const obj of res.data) {
				options.url = `${options.origin_url}/rest/agile/1.0/board/${obj.rapidViewId}/sprint`;
				options.projectId = obj.id;
				options.projectName = obj.name;
				options.rapidViewId = obj.rapidViewId;

				let result = await request.initialize(options);
				if (result == null) {
					continue;
				}

				if (result.values) {
					if (result.values.length > 0 ) {
						result.values.filter(el => el.state != "feature");
						result.values.map(el => {
							el.projectId = options.projectId;
							el.projectName = options.projectName;
							el.rapidViewId = options.rapidViewId;
						});
						data = data.concat(result.values);
					}
				}
			}

			for (const sprint of data) {
				options.url = `${options.origin_url}/rest/greenhopper/1.0/rapid/charts/sprintreport?rapidViewId=${sprint.originBoardId}&sprintId=${sprint.id}`;
				
				let response = await request.initialize(options);
				if (response == null) {
					continue;
				}

				if (response) {
					getDataSprint(response, sprint, next);
				}
			}
		}
	});

}

