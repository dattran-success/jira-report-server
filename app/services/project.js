'use strict';

let Project = require('../models/project.model');
let baseService = require('./base');
let request = require("../helpers/request");
let utils = require("../helpers/utils");

exports.upsert = ( id, update, next) => {
    baseService.findOneAndUpdate('Project', { id: id }, update, { new: true, upsert: true }, (errUpdate, res) => {
		if (errUpdate) {
			return next(errUpdate);
		}

		return next(null, res);
	});
}

exports.add = (project, next) => {
	let newProject = new Project(project);
	// set the user's local credentials
	newProject.provider = 'local';

	newProject.save((err) => {
		if (err) {
			return next(err);
		}
		if(next) {
			return next(null, newProject);
		}
	});
};

exports.createProjectFromJira = ( queries = {}, options, next) => {
	baseService.paginate('RapidView', queries, options, async(error, res) => {
		if (error) {
			return next(error);
		}

		if (res) {

			let result = await request.initialize(options);
			
			if (result) {

				result = result.filter(el => el.id > 14260);

				let projects = await result.map((obj, i) => {
					let rapidView = res.data.filter((x) => {
						if (obj.id == x.location.projectId) {
							return x.id;
						}
					});

					obj.rapidView = [];
					obj.rapidViewId = 0;
					obj.flag = utils.customizeName(obj.name);
					obj.active = true;

					if (rapidView.length > 0) {
						obj.rapidView = rapidView;
						obj.rapidViewId = rapidView[0].id;
					}

					return obj;
				});

				for (let project of projects) {
					this.upsert(project.id, project, (err, response) => {
						if (err) {
							console.log(err);
						}
					});
				}

				let response = {
					status: true,
					message: `Processing... ${projects.length} documents into ProjectSchemas`
				}

				return next(null, response);
			}
		}
	});

}
