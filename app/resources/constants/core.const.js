module.exports = {
	'ACTIVATE_TEMPLATE': 'activate-email.html',
	'SYS_ERR_TEMPLATE': 'sys-error.html',
	'APP_NAME': "Express MVC",
	'LOCAL_LOGIN': 'local',
	'ADMIN_LOGIN': 'admin-login',
	'HTTP_CODE':{
		'NOT_FOUND': 404,
		'BAD_REQUEST': 400,
		'UNAUTHORIZED': 401,
		'UNPROCESSABLE_ENTITY': 422,
		'OK': 200,
		"INTERNAL_SERVER": 500
	},
	'REQUEST_TYPE': {
		'GET': 'GET',
		'POST': 'POST',
		'PUT': 'PUT',
		'DEL': 'DELETE'
	},
	'FIELDS': {
		'SPRINT': "customfield_10006",
		'WEIGHTED_DEFECT': "customfield_10205",
		'ESTIMATE_STORY_POINT': "customfield_10004",
		'ISSUE_TYPE': "issuetype",
		'STATUS': "status",
		'SUMMARY': "summary",
		'PROJECT': "project",
		'CREATED': "created",
		'LABELS': "labels"
	},
	'ISSUES': {
		'DEFECT': 'Defect',
		'STORY': 'Story',
		'CLOSED': 'Closed',
		'NC': 'NC',
		'DONE': 'Done',
		'BUG': 'Bug',
		'DEFAULT_DATE': '2019-10-10',
		'DEFAULT_SPRINT_ID': 10000
	}
};
