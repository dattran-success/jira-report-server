
// Load Module dependencies.
let version = require('../controllers/version');

module.exports = (router) => {
	router.route('/version')
		.get( version.getVersions );
};
