
// Load Module dependencies.
let rapidView = require('../controllers/rapid.view');

module.exports = ( router) => {
	router.route('/rapid-view')
		.get( rapidView.getRapidViews )
		.post( rapidView.create );
};
