
// Load Module dependencies.
let sprint = require('../controllers/sprint');

module.exports = ( router) => {
	router.route('/sprint')
		.get( sprint.getSprint );
};
