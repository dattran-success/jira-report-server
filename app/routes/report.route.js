
// Load Module dependencies.
let report = require('../controllers/report');

module.exports = ( router) => {
	router.route('/report')
		.get( report.getReport );
};
