
// Load Module dependencies.
let core = require('../controllers/core');

module.exports = ( router) => {
	router.route('/user-exists/:email')
		.get( core.checkUserEmail );
};
