
// Load Module dependencies.
let issue = require('../controllers/issue');

module.exports = ( router) => {
	router.route('/issue')
		.get( issue.getIssue );
};
