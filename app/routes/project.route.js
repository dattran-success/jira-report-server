
// Load Module dependencies.
let project = require('../controllers/project');

module.exports = ( router) => {
	router.route('/project')
		.get( project.getProject );
};
