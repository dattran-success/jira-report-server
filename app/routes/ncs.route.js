
// Load Module dependencies.
let nc = require('../controllers/ncs');

module.exports = ( router) => {
	router.route('/nc')
		.get( nc.getNCs );
};
