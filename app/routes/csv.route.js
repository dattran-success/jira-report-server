
// Load Module dependencies.
let csv = require('../controllers/csv');

module.exports = ( router) => {
	router.route('/csv')
		.post( csv.upload );
};
