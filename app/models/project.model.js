"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	RapidView = require('./rapid.view.model'),
	Schema = mongoose.Schema;

let ProjectSchema = new Schema({
	id: {
		type: Number
	},
	name: String,
	key: String,
	flag: String,
	active: {
		type: Boolean,
		default: true,
	},
	rapidView: {
		type: [RapidView.RapidViewSchema],
		default: []
	},
	rapidViewId: Number,
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
ProjectSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;
	next();
} );

// generating a hash
ProjectSchema.methods.generateHash = function(flag) {
	return bcrypt.hashSync(flag, bcrypt.genSaltSync(6), null);
};

mongoose.model( "Project", ProjectSchema, config.dbTablePrefix.concat( "projects" ) );
