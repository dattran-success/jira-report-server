"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	Schema = mongoose.Schema,
	SprintSchema = require('./sprint.model');

const SummaryDataSchema = new mongoose.Schema({
	date: Number,
	total_estimated_story_point: Number,
	total_story_point_resolved: Number
}, { _id : false });

const DefectDensitySchema = new mongoose.Schema({
	sprint: String,
	weightedDefect: Number,
	sp: Number,
	defectDensity: Number,
	accumulatedDefectDensity: Number
}, { _id : false });

const VelocityIncreaseSchema = new mongoose.Schema({
	sprint: String,
	storyDelivered: Number,
	averageVelocity: Number,
	velocityIncrease: Number,
	velocityVariance: Number
}, { _id : false });

const DefectDensityByDateSchema = new mongoose.Schema({
	date: Number,
	defectCount: Number,
	weightedDefect: Number,
	accumulatedDefectDensity: Number,
	defect_density: Number
}, { _id : false });

const DeliveredAndSpDataSchema = new mongoose.Schema({
	id: Number,
	name: String,
	story_delivered: Number,
	startDate: Number
}, { _id : false });

let ReportSchema = new Schema({
	id: {
		type: Number
	},
	project: String,
	flag: String,
	summaryData: [SummaryDataSchema],
	storiesCompletionRatio: [SprintSchema],
	defectDensity: [DefectDensitySchema],
	velocityIncrease: [VelocityIncreaseSchema],
	updateAt: Number,
	version: String,
	releaseDateLatest: Number,
	defectDensityByDate: [DefectDensityByDateSchema],
	deliveredAndSpData: [DeliveredAndSpDataSchema],
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
ReportSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;
	

	next();
} );

mongoose.model( "Report", ReportSchema, config.dbTablePrefix.concat( "reports" ) );
