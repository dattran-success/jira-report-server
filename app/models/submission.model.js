"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	Schema = mongoose.Schema;

let SubmissionSchema = new Schema({
	id: {
		type: Number
	},
	description: String,
	name: String,
	archived: Boolean,
	released: Boolean,
	startDate: String,
	releaseDate: String,
	overdue: Boolean,
	userReleaseDate: String,
	projectId: {
		type: Number,
		ref: 'Project'
	},
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
SubmissionSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;

	next();
} );

mongoose.model( "Submission", SubmissionSchema, config.dbTablePrefix.concat( "submissions" ) );
