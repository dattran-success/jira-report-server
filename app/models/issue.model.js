"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	Schema = mongoose.Schema;

let IssueSchema = new Schema({
	id: {
		type: Number
	},
	key: String,
	fields: {
		issuetype: {
			id: Number,
			name: String
		},
		customfield_10006: [String],
		customfield_10004: Number,
		customfield_10205: Number,
		created: Number,
		project: {
			id: Number,
			key: String,
			name: String
		},
		status: {
			id: Number,
			name: String
		},
		labels: []
	},
	sprint: {
		id: Number,
		name: String,
		state: String,
		startDate: Number
	},
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
IssueSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;

	next();
} );

mongoose.model( "Issue", IssueSchema, config.dbTablePrefix.concat( "issues" ) );
