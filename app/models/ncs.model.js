"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	Schema = mongoose.Schema;

let NCsSchema = new Schema({
	id: {
		type: Number
	},
	project: String,
	status: Object,
	area: Object,
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
NCsSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;

	next();
} );

mongoose.model( "NCs", NCsSchema, config.dbTablePrefix.concat( "ncs" ) );
