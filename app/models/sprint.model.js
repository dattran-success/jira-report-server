"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	bcrypt = require('bcrypt-nodejs'),
	Schema = mongoose.Schema;

let SprintSchema = new Schema({
	id: {
		type: Number
	},
	startDate: Number,
	sprintId: {
		type: Number,
		ref: 'Sprint'
	},
	sprintName: String,
	projectName: String,
	projectId: {
		type: Number,
		ref: 'Project'
	},
	storyCompletionRatio: Number,
	committedIssues: Number,
	completedIssues: Number,
	rapidViewId: {
		type: Number,
		ref: 'RapidView'
	},
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
SprintSchema.pre( "save", ( next ) => {

	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}

	this.updated_time = now;
	

	next();
} );

mongoose.model( "Sprint", SprintSchema, config.dbTablePrefix.concat( "sprints" ) );
