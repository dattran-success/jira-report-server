"use strict";

/**
 * Module dependencies.
 */

let mongoose = require( "mongoose" ),
	path = require('path'),
	config = require(path.resolve('./config')),
	moment = require('moment'),
	Schema = mongoose.Schema;

let RapidViewSchema = new Schema({
	id: {
		type: Number
	},
	name: String,
	location: {
		projectId: Number,
		displayName: String,
		projectName: String,
		projectKey: String,
		name: String
	},
	created_time: Number,
	updated_time: Number
}, {
	_id: false,
	autoIndex: config.dbAutoIndex,
	validateBeforeSave: false
} );

/**
 * Pre-save hook
 */
RapidViewSchema.pre( "save", ( next ) => {
	// this.increment();
	let now = +moment.utc();

	if(this.isNew) {
		this.created_time = now;
	}
	this.updated_time = now;
	next();
} );

mongoose.model( "RapidView", RapidViewSchema, config.dbTablePrefix.concat( "rapid_views" ) );

exports.RapidViewSchema = RapidViewSchema;
