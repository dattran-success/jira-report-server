let axios = require('axios'),
	config = require('../../config');

const axiosInstance = axios.create({
	baseURL: `${config.host}:${config.port}`,
	headers: {
		'Authorization': `${config.jira.token_type} ${config.jira.secret}`
	},
	withCredentials: true,
});

module.exports = {
	axiosInstance
}
