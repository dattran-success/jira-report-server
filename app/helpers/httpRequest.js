exports.optionRequest = (method, url) => {
    return {
        method: method,
        url: url,
        headers: {
            'Authorization': 'Basic ' + process.env.API_JIRA_TOKEN,
            'Accept': 'application/json'
        }
    };
}