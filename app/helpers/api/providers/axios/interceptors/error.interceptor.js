let constants = require('../../../../../resources/constants');

function ErrorIntercept(response) {
	response.use(
		data => {
			return data;
		},
		(err) => {
			console.log(err);
			// if (err.response && err.response.status === constants.HTTP_CODE.UNAUTHORIZED) {
			// 	// Handle error
			// }

			// return Promise.reject(err);
		},
	);
}

function handleApiError(error) {
	// console.log(error);
	const res = error.response || error.message || error;

	if (res === 'Network Error') {
		let response = { error: `There were some errors with your request: ${res}` };
		throw response;
	}

	if (res.status === constants.HTTP_CODE.UNPROCESSABLE_ENTITY && res.data) {
		const errors = [];

		Object.keys(res.data.errors).forEach((field) => {
			res.data.errors[field].forEach((err) => {
				errors.push(err);
			});
		});

		res.error = `There were some errors with your request: ${errors.join(', ')}`;
	}

	if (res.status === constants.HTTP_CODE.UNAUTHORIZED) {
		res.error = 'Your session has timed out. Please log in again.';
	}

	if (res.status === constants.HTTP_CODE.BAD_REQUEST) {
		res.error = res.data.error;
	}

	if (!res.error) {
		res.error = (res.data && (res.data.message || res.data.errors)) || res.statusText || String(res);
	}

	throw res;
}

module.exports = {
	ErrorIntercept,
	handleApiError
}
