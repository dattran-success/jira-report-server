let appConf = require('../../../../../../config');

exports.JwtIntercept = (request) => {
	request.use((config) => {
		const method = config.method.toUpperCase();
		const newConfig = {
			...config,
			headers: {
				'Authorization': `${appConf.jira.token_type} ${appConf.jira.secret}`,
			}
		};

		if (method === 'PUT' || method === 'DELETE' || method === 'PATCH') {
			newConfig.headers['X-HTTP-Method-Override'] = method;
			newConfig.method = 'post';
		}

		return newConfig;
	});
}
