let axios = require('./init'),
	formDataCompiler = require('../../../formDataCompiler'),
	error = require('./interceptors/error.interceptor'),
	constants = require('../../../../resources/constants');

/**
 * Requests to API
 * @param {String} type
 * @param {String} url
 * @param {Object} queryParam
 * @param {Object} data
 * @param {import('axios').AxiosRequestConfig} options
 * If you wish to send FormData instead of JSON, set options.useFormData to true.
 */
const request = async(url, options = { method: constants.REQUEST_TYPE.GET, useFormData: false }) => {
	let data = { ...(options.data || {}) };
	const reqOptions = { ...options, data };
	if ([ constants.REQUEST_TYPE.POST, constants.REQUEST_TYPE.PUT ].includes(options.method) && options.useFormData) {
		reqOptions.data = formDataCompiler.ObjectToFormData(data);
		reqOptions.headers = { 'Content-Type': 'multipart/form-data' };
	}
	let req = axios.axiosInstance(url, reqOptions);
	return req.catch(error.handleApiError);
};

/**
 * Send GET Request to API
 * @param {String} url
 * @param {AxiosRequestConfig} options
 */
const get = (url, options = {}) => (
	request(url, { ...options, method: constants.REQUEST_TYPE.GET })
);

/**
 * Send POST Request to API
 * @param {String} url
 * @param {Object} data
 * @param {AxiosRequestConfig} options
 * If you wish to send FormData instead of JSON, set options.useFormData to true.
 */
const post = (url, data = {}, options = {}) => (
	request(url, { ...options, data, method: constants.REQUEST_TYPE.POST })
);

/**
 * Send PUT Request to API
 * @param {String} url
 * @param {Object} data
 * @param {AxiosRequestConfig} options
 * If you wish to send FormData instead of JSON, set options.useFormData to true.
 */
const put = (url, data = {}, options = {}) => (
	request(url, { ...options, data, method: constants.REQUEST_TYPE.PUT })
);

/**
 * Send DELETE Request to API
 * @param {String} url
 * @param {Object} queryParam
 * @param {AxiosRequestConfig} options
 */
const remove = (url, options = {}) => (
	request(url, { ...options, method: constants.REQUEST_TYPE.DEL })
);

module.exports = {
	get,
	post,
	put,
	delete: remove,
}