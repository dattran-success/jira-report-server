let axios = require('axios'),
	config = require('../../../../../config'),
	jwt = require('./interceptors/jwt.interceptor'),
	error = require('./interceptors/error.interceptor');

const axiosInstance = axios.create({
	baseURL: `${config.host}:${config.port}`,
	headers: {
	},
	withCredentials: true,
});

const registerInterceptors = (axiosInst) => {
	jwt.JwtIntercept(axiosInst.interceptors.request);
	error.ErrorIntercept(axiosInst.interceptors.response);
};

registerInterceptors(axiosInstance);

module.exports = {
	axiosInstance
}
