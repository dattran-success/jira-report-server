let request = require('request'),
	config = require('../../config'),
	constants = require('../resources/constants');

function optionRequest(opt) {
	return {
		method: opt.method,
		uri: opt.url,
		headers: {
			'Authorization': `${config.jira.token_type} ${config.jira.secret}`,
			'Accept': 'application/json'
		}
	};
}

function initialize(opt) {
	const options = optionRequest(opt);
	return new Promise((resolve, reject) => {
		request(options, (error, response, body) => {
			if (!error && response.statusCode == constants.HTTP_CODE.OK) {
				resolve(JSON.parse(body));
			}
			else {
				console.log(`Request exception: ${error}`);
				resolve(null);
			}
		});

	});
}

module.exports = {
	initialize
};
