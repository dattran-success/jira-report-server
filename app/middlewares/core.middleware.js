'use strict';

/**
 * Module dependencies.
 */
let path = require('path'),
	passport = require('passport'),
	morgan = require('morgan'),
	bodyParser = require('body-parser'),
	compress = require('compression'),
	methodOverride = require('method-override'),
	config = require('../../config'),
	cors = require('cors'),
	cookieParser = require('cookie-parser'),
	graphqlHTTP = require('express-graphql'),
	schema = require('../graphql/schemas'),
	cron = require('node-cron'),
	request = require('../helpers/request'),
	constants = require('../resources/constants'),
	utils = require('../helpers/utils'),
	issueService = require('../services/issue'),
	rapidViewService = require('../services/rapid.view'),

	moment = require("moment");

/**
 * Initialize application middleware
 */
module.exports = (app) => {

	//set view engine
	app.set('views', path.resolve( './app/views'));
	app.engine('html', require('ejs').renderFile);
	app.set('view engine', 'ejs');

	// Showing stack errors
	app.set('showStackError', true);

	// Enable jsonp
	app.enable('jsonp callback');

	// Should be placed before express.static
	app.use(compress({
		filter: function(req, res) {
			return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
		},
		level: 9
	}));

	// Environment dependent middleware
	if (process.env.NODE_ENV === 'development') {
		// Enable logger (morgan)
		app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		app.locals.cache = 'memory';
	}

	// for parsing application/json
	app.use(bodyParser.json({
		verify: function(req, res, buf) {
			// get raw_json_body
			req.raw_json_body = buf.toString();
			try {
				JSON.parse(req.raw_json_body);
			} catch (e) {
				throw new TypeError('Input data invalid');
			}
		},
		limit: '15mb'
	}));

	// for parsing application/www-urlencoded
	app.use(bodyParser.urlencoded({
		extended: true,
		limit: '15mb'
	}));

	// Request body parsing middleware should be above methodOverride
	app.use(methodOverride('X-HTTP-Method')); // Microsoft
	app.use(methodOverride('X-HTTP-Method-Override')); // Google/GData
	app.use(methodOverride('X-Method-Override')); // IBM

	// Add the cookie parser and flash middleware
	app.use(cookieParser(config.jwt.secret));

	// enable CORS
	// Enable preflight requests for all routes
	app.use(cors({
		origin: true,
		credentials: true
	}));

	app.use(passport.initialize());

	// load swagger
	if (process.env.NODE_ENV !== 'test') {

		let swaggerUi = require('swagger-ui-express'),
			swaggerDocument = require(path.resolve('./config/swagger'));

		app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
	}

	// graphql
	app.use('/graphql', cors(), graphqlHTTP({ schema: schema, graphiql: true }));

	// crontab run every day at 00:00:00
	// don't change this ordinal crond
	cron.schedule('2 10 * * *', async(req, res, next) => {
		let origin_url = `${config.jira.uri}`;
		let url = origin_url;
		let method = constants.REQUEST_TYPE.GET;
		let limit = config.pagination.perPage;
		let query = {};
		let rapidViewOpts = {
			url: `${url}/rest/agile/1.0/board`,
			origin_url: `${url}/rest/agile/1.0/board`,
			method,
			limit
		};

		await rapiViewService.getRapidViewByAPI(query, rapidViewOpts, async(error, response) => {
			if (error) {
				return next(error);
			}

			console.log(response);
		});

		await utils.sleep(10000);

		let projectOpts = {
			url: `${url}/rest/api/3/project`,
			origin_url: `${url}/rest/api/3/project`,
			method,
			limit
		};

		await projectService.createProjectFromJira(query, projectOpts, (err, result) => {
			if(err) {
				return next(err);
			}

			console.log(result);
		});

		await utils.sleep(10000);

		await versionService.createVersionFromAPI(query, projectOpts, async (err, result) => {
			if(err) {
				return next(err);
			}

			console.log(result);
		});

		await utils.sleep(30000);

		let options = {
			url,
			origin_url,
			method,
			limit
		};
		
		await sprintService.createSprintFromAPI(query, options, (err, result) => {
			if(err) {
				return next(err);
			}

			console.log(result);
		});

		console.log("Crontab 00:00:00 task [rapid view, project, version, sprint] are running...");

	});

	cron.schedule('4 9 * * *', (req, res, next) => { // run after 10min
		
		let origin_url = `${config.jira.uri}`;
		let url = origin_url;
		let created = null;
		// let created = moment().subtract(1, 'days').format('YYYY/MM/DD');
		let fields = [
			constants.FIELDS.ISSUE_TYPE,
			constants.FIELDS.STATUS,
			constants.FIELDS.SUMMARY,
			constants.FIELDS.PROJECT,
			constants.FIELDS.CREATED,
			constants.FIELDS.LABELS,
			constants.FIELDS.SPRINT,
			constants.FIELDS.WEIGHTED_DEFECT,
			constants.FIELDS.ESTIMATE_STORY_POINT
		];
		let method = constants.REQUEST_TYPE.GET;
		let limit = config.pagination.perPage;
		let max_result = config.jira.limit;
		let query = {};

		let options = {
			url,
			origin_url,
			method,
			limit,
			fields,
			created,
			max_result
		};
		
		issueService.createIssueFromAPI(query, options, (err, result) => {
			if(err) {
				return next(err);
			}

			return next(null, result);
		});

		console.log("Crontab 00:10:00 task [issue] is running...");

	});

	cron.schedule('10 9 * * *', async () => { // run after 15min
		let url = `${config.protocol}://${config.host}:${config.port}${config.basePath}`;
		let method = constants.REQUEST_TYPE.GET;

		await request.initialize({ url: `${url}/report`, method: method }).then(async(res) => {
			console.log(res);
			await utils.sleep(30000);
		}).catch(err => {
			console.log(err);
		});

		await request.initialize({ url: `${url}/nc`, method: method }).then(async(res) => {
			console.log(res);
		}).catch(err => {
			console.log(err);
		});

		console.log("Crontab 00:15:00 task [report, ncs] are running...");
	});
};
