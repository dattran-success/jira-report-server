let {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLFloat,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLBoolean
} = require('graphql');
let moment = require('moment');
let baseService = require("../services/base");
let SurveySchema = require("../models/survey.model");
let SubmissionSchema = require("../models/submission.model");

let storiesCompletionRatioType = new GraphQLObjectType({
    name: 'storiesCompletionRatio',
    fields: () => {
        return {
            projectId: {
                type: GraphQLString
            },
            projectName: {
                type: GraphQLString
            },
            sprintName: {
                type: GraphQLString
            },
            sprintId: {
                type: GraphQLFloat
            },
            completedIssues: {
                type: GraphQLFloat
            },
            committedIssues: {
                type: GraphQLFloat
            },
            storyCompletionRatio: {
                type: GraphQLFloat
            },
            startDate: {
                type: GraphQLFloat
            }
        }
    }
});

let defectDensityType = new GraphQLObjectType({
    name: 'defectDensity',
    fields: () => {
        return {
            sprint: {
                type: GraphQLString
            },
            weightedDefect: {
                type: GraphQLFloat
            },
            sp: {
                type: GraphQLFloat
            },
            defectDensity: {
                type: GraphQLFloat
            },
            accumulatedDefectDensity: {
                type: GraphQLFloat
            },
            
        }
    }
});

let velocityType = new GraphQLObjectType({
    name: 'velocity',
    fields: () => {
        return {
            sprint: {
                type: GraphQLString
            },
            storyDelivered: {
                type: GraphQLFloat
            },
            averageVelocity: {
                type: GraphQLFloat
            },
            velocityIncrease: {
                type: GraphQLFloat
            },
            velocityVariance: {
                type: GraphQLFloat
            }
        }
    }
});

let summaryReportType = new GraphQLObjectType({
    name: 'summaryReport',
    fields: () => {
        return {
            date: {
                type: GraphQLFloat
            },
            total_estimated_story_point: {
                type: GraphQLFloat
            },
            total_story_point_resolved: {
                type: GraphQLFloat
            }
        }
    }
});

let defectDensityByDateType = new GraphQLObjectType({
    name: 'defectDensityByDate',
    fields: () => {
        return {
            date: {
                type: GraphQLFloat
            },
            defectCount: {
                type: GraphQLFloat
            },
            weightedDefect: {
                type: GraphQLFloat
            },
            accumulatedDefectDensity: {
                type: GraphQLFloat
            },
            defect_density: {
                type: GraphQLFloat
            },
            
        }
    }
});

let deliveredAndSpDataType = new GraphQLObjectType({
    name: 'deliveredAndSpData',
    fields: () => {
        return {
            id: {
                type: GraphQLString
            },
            name: {
                type: GraphQLString
            },
            story_delivered: {
                type: GraphQLFloat
            }
        }
    }
});

let reportType = new GraphQLObjectType({
    name: 'report',
    fields: () => {
        return {
            id: {
                type: GraphQLString
            },
            project: {
                type: GraphQLString
            },
            flag: {
                type: GraphQLString
            },
            updateAt: {
                type: GraphQLFloat
            },
            releaseDateLatest: {
                type: GraphQLFloat
            },
            version: {
                type: GraphQLString
            },
            summaryData:{
                type: new GraphQLList(summaryReportType),
                resolve: (root) => {
                    return root.summaryData;
                }
            },
            storiesCompletionRatios: {
                type: new GraphQLList(storiesCompletionRatioType),
                resolve: (root) => {
                    return root.storiesCompletionRatio;
                }
            },
            defectDensities: {
                type: new GraphQLList(defectDensityType),
                resolve: (root) => {
                    return root.defectDensity;
                }
            },
            velocities: {
                type: new GraphQLList(velocityType),
                resolve: (root) => {
                    return root.velocityIncrease;
                }
            },
            defectDensitiesByDate: {
                type: new GraphQLList(defectDensityByDateType),
                resolve: (root) => {
                    return root.defectDensityByDate;
                }
            },
            deliveredAndSpData: {
                type: new GraphQLList(deliveredAndSpDataType),
                resolve: (root) => {
                    return root.deliveredAndSpData;
                }
            },
        }
    }
});

let serveyType = new GraphQLObjectType({
    name: 'servey',
    fields: () => {
        return {
            flag: {
                type: GraphQLString
            },
            project: {
                type: GraphQLString
            },
            organization: {
                type: GraphQLString
            },
            product_quality: {
                type: GraphQLFloat
            },
            skill_and_expertise: {
                type: GraphQLFloat
            },
            project_schedule: {
                type: GraphQLFloat
            },
            communication: {
                type: GraphQLFloat
            },
            professtionalism: {
                type: GraphQLFloat
            },
            likely: {
                type: GraphQLFloat
            },
            comments: {
                type: GraphQLString
            },
            createdAt: {
                type: GraphQLFloat
            },
            _id: {
                type: GraphQLString
            }
        }
    }
});

let projectType = new GraphQLObjectType({
    name: 'project',
    fields: () => {
        return {
            id: {
                type: GraphQLString
            },
            name: {
                type: GraphQLString
            },
            key: {
                type: GraphQLString
            },
            flag: {
                type: GraphQLString
            },
            rapidViewId: {
                type: GraphQLInt
            },
            action: {
                type: GraphQLBoolean
            }
        }
    }
});

let submissionType = new GraphQLObjectType({
    name: 'submission',
    fields: () => {
        return {
            flag: {
                type: GraphQLString
            },
            project: {
                type: GraphQLString
            },
            createdAt: {
                type: GraphQLFloat
            },
            submitted: {
                type: GraphQLBoolean
            }
        }
    }
});

let queryType = new GraphQLObjectType({
    name: 'Query',
    fields: () => {
        return {
            getDataReport: {
                type: new GraphQLList(reportType),
                args: {
                    flag: {
                        type: GraphQLString
                    }
                },
                resolve: async(root, params) => {
                    return await baseService.query('Report', { flag: params.flag });
                }
            },
            getProject: {
                type: projectType,
                args: {
                    flag: {
                        type: GraphQLString
                    }
                },
                resolve: async(root, params) => {
                    let result = await baseService.query('Project', { flag: params.flag });

                    if (result.length > 0) {
                        return result[0];
                    } else {
                        return null;
                    }

                }
            },

            getDataSurvey: {
                type: new GraphQLList(serveyType),
                args: {
                    flag: {
                        type: GraphQLString
                    }
                },
                resolve: async(root, params) => {
                    return await baseService.query('Servey', { flag: params.flag });
                }
            },

            getSubmissionByDate: {
                type: submissionType,
                args: {
                    flag: {
                        type: GraphQLString
                    }
                },
                resolve: async(root, params) => {
                    let query = {
                        flag: params.flag,
                        createdAt: moment(moment().format('YYYY-MM-DD')).valueOf()
                    };
                    
                    return await baseService.query('Submission', query);
                }
            },
            getNCs: {
                type: new GraphQLList(ncType),
                resolve: async(root, params) => {
                    return await baseService.query('NCs', {});
                }
            }
        }
    }
});

var mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: function() {
        return {
            addServey: {
                type: serveyType,
                args: {
                    flag: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    project: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    organization: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    product_quality: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    skill_and_expertise: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    project_schedule: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    communication: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    professtionalism: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    likely: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    comments: {
                        type: GraphQLString
                    },
                    createdAt: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    }
                },
                resolve: async (root, params) => {
                    let newSurvey = new SurveySchema(params);
                    newSurvey = await newSurvey.save();
                    
                    if (newSurvey && newSurvey.ops) {
                        let newSubmission = new SubmissionSchema({
                            submitted: true,
                            flag: params.flag,
                            project: params.project,
                            createdAt: params.createdAt
                        });
                        newSubmission = await newSubmission.save();

                        if (newSubmission) {
                            return newSurvey.ops[0];
                        }
                    }

                    return null;
                }
            }
        };
    }
});

let ncType = new GraphQLObjectType({
    name: 'ncs',
    fields: () => {
        return {
            id: {
                type: GraphQLString
            },
            project: {
                type: GraphQLString
            },
            area: {
                type: areaType,
                resolve: (root) => {
                    return root.area;
                }
            },
            status: {
                type: statusType,
                resolve: (root) => {
                    return root.status;
                }
            }
        }
    }
});

let areaType = new GraphQLObjectType({
    name: 'area',
    fields: () => {
        return {
            RE: {
                type: GraphQLInt
            },
            AD: {
                type: GraphQLInt
            },
            IM: {
                type: GraphQLInt
            },
            TE: {
                type: GraphQLInt
            },
            DE: {
                type: GraphQLInt
            },
            DAR: {
                type: GraphQLInt
            },
            CAR: {
                type: GraphQLInt
            },
            PM: {
                type: GraphQLInt
            },
            CM: {
                type: GraphQLInt
            }
        }
    }
});

let statusType = new GraphQLObjectType({
    name: 'status',
    fields: () => {
        return {
            Open: {
                type: GraphQLInt
            },
            Implemented: {
                type: GraphQLInt
            },
            Closed: {
                type: GraphQLInt
            },
            Overdue: {
                type: GraphQLInt
            }
           
        }
    }
});

module.exports = new GraphQLSchema({ query: queryType, mutation: mutation });
